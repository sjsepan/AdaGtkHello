with Gtk.Main;
with Gtk.Window;      use Gtk.Window;
with Gtk.Widget;     use Gtk.Widget;
with Gtk.Button;      use Gtk.Button;
with Gtkada.Handlers; use Gtkada.Handlers;
with Glib.Object;

--with Main_quit;
with gtkhello_callbacks; use gtkhello_callbacks;

procedure gtkhello is
   FrmMain : Gtk_Window;
   BtnQuit : Gtk_Button;

begin
   --  Initialize GtkAda.
   Gtk.Main.Init;

   -- create a top level window
   Gtk_New (FrmMain);
   FrmMain.Set_Title ("Gtk Hello");

   -- When the window emits the "delete-event" signal (which is emitted
   --  by GTK+ in response to an event coming from the window manager,
   --  usually as a result of clicking the "close" window control), we
   --  ask it to call the on_delete_event() function as defined above.
   -- This can also be triggered by a button (in this case) or menu click.
   FrmMain.On_Delete_Event (FrmMain_OnDeleteEvent'Access);

   -- connect the "destroy" signal
   -- Note: does not call Gtk.Main.Main_Quit directly
   FrmMain.On_Destroy (FrmMain_OnDestroy'Access);

   -- set the (interior) border width (margins) of the window (client area)
   FrmMain.Set_Border_Width (10);

   -- create a button with label
   Gtk_New (BtnQuit, "Quit");

   -- Connect the click signal
   -- Note:this does not do nothing, while the Widget_Callback.Object_Connect call below
   --  connects an alternate click event receiving the main form object
   BtnQuit.On_Clicked (BtnQuit_OnClicked'Access);

   -- Connect the "clicked" signal of the button to a callback function,
   --  that will close the Win when button is clicked.
   -- This is the alternate version of the callback, 
   --  that passes the main form object instead of the button object.
   Widget_Callback.Object_Connect (
      BtnQuit,
      "clicked",
      Widget_Callback.To_Marshaller (BtnQuit_OnClicked2'Access),
      FrmMain
   );
   

   -- This packs the button into the window. A Gtk_Window inherits from Gtk_Bin
   -- which is a special container that can only have one child.
   FrmMain.Add (BtnQuit);

   --  Show window and all descendent widgets
   FrmMain.Show_All;

   -- All GTK applications must have a Gtk.Main.Main. This is the main message loop.
   -- Control ends here and waits for an event to occur 
   -- (like a key press or a mouse event),  until Gtk.Main.Main_Quit is called.
   Gtk.Main.Main;
end gtkhello;
